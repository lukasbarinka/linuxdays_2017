# bash and Mario Bros.

This is a talk about pipes in UNIX and especialy in shell at LinuxDays 2017 conference. It starts with basic pipes usage e.g. redirections, stdin/out and ends with proces substitutions, named pipes and co-proceses. Some parts are bash-specific.

![Title slide](title.png?raw=true "Title slide in PNG")

The [final presentation in SVG](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg) is accessible at my Gitlab Pages.

You can also see the talk at conference video at YouTube. The talk is in czech language.

[![bash and Mario Bros. at LinuxDays 2017 conference](https://img.youtube.com/vi/nzj07TQt6q8/0.jpg)](https://www.youtube.com/watch?v=nzj07TQt6q8).

## About Presentation

This presentation uses SVG format. It is created using Inkscape and sozi extenstion. The workflow is simple, it consists of a few steps:

  1. Pictures are hand-drawn using paper and pen,
  2. Papers are scanned using Gimp,
  3. Bitmap images are vectorised using Inkscape,
  4. Vectoried parts are used to create slides (FullHD, 16:9) and
  5. Presentation is made from slides using Sozi extenstion in Inkscape.

![Backstage](backstage.png?raw=true "Sources")

## Table of Contents

The talk takes 50 minutes with 43 slides. The topics are divided into 9 chapters. You can use links to go directly into these topics.

  1. [Pipe](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#pipe)
  2. [Filters](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#filters)
  3. [I/O](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#io)
  4. [Redirection](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#redirection)
  5. [Files](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#files)
  6. [Pipeline](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#Pipeline)
  7. [Process Substitution](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#process_substituion)
  8. [Buffering](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#buffering)
  9. [Coprocesses](https://lukasbarinka.gitlab.io/bash_and_mario_bros.svg#coprocesses)

## Authors

* **Lukáš Bařinka** - [gitlab](https://gitlab.com/lukasbarinka), [gitlabpages](https://lukasbarinka.gitlab.io)

## License

This project is released under GNU GPLv3 - see the gpl-3.0.txt file for details.

